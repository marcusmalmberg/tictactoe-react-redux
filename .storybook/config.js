import { configure } from '@kadira/storybook';

import '../src/index.css';

const reqs = require.context('../src/', true, /\.story\.js$/);

function loadStories() {
  reqs.keys().forEach(reqs)
}

configure(loadStories, module);

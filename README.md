# Tic-Tac-Toe

[![Codeship Status for marcusmalmberg/tictactoe-react-redux](https://app.codeship.com/projects/17559680-f60a-0134-43c9-62cc51a71676/status?branch=master)](https://app.codeship.com/projects/210420)

This project is a basic implementation of the classic game Tic-Tac-Toe in React with Redux.

Demo: https://tic-tac-toe-b84a6.firebaseapp.com/

---

Key parts:

- React with Redux
- ES2015+
- Testing with Jest and Enzyme
- Showcasing all components with Storybook
- CI/CD with Codeship

---

Order of execution:

1. The project was bootstrapped with `create-react-app`
2. `redux`, `react-redux` and `classNames` was added
3. Implemented basic `App`, `Board` and `Square` components
4. Applied some styling
5. Implemented actions
6. Implemented win conditions and playstates
7. Added a `Result` component to display current player and a "Restart" button when the game is over
8. Implemented tests with `Jest` and `Enzyme`
9. Added `react-storybook` and created stories for all components
10. Configured CI/CD with Codeship and Firebase

---

Random thoughts:

- I'm only using functional components
- Didn't do TDD, wrote all specs after the code was implemented.
- Only `App` is a connected component. You might connect the `Result` and `Board` components instead of passing the props.
- The calculations for the win conditions are in the reducer. It might be a good idea to extract them to some util functions in another file. That would clean up the reducer and make both parts easier to test.
- Only supports player vs player (no AI). Might be a good exercise to implement that later on.
- Does not use BEM in the CSS (or any other fancy methodology). In hindsight that might have been a good idea since it's a showcase project.

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

import * as actions from './actions';
import Board from './components/Board';
import Result from './components/Result';

export const App = (props) => {
  const { playState, player, squares } = props;
  const { markSquare, restartGame } = props;

  return (
    <div>
      <Board squares={squares} playState={playState} onSquareClick={markSquare} />
      <Result player={player} playState={playState} restartGameClick={restartGame} />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    squares: state.game.squares,
    playState: state.game.playState,
    player: state.game.player,
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

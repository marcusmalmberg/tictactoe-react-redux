import React from 'react';
import { storiesOf, action, linkTo } from '@kadira/storybook';

import Board from './Board';
import Marker from '../constants/marker'
import PlayState from '../constants/playState'

const setup = () => {
  const state = {
    playState: PlayState.RUNNING,
    player: Marker.X,
    squares: [
      { id: 0, marker: Marker.OPEN },
      { id: 1, marker: Marker.OPEN },
      { id: 2, marker: Marker.OPEN },
      { id: 3, marker: Marker.OPEN },
      { id: 4, marker: Marker.OPEN },
      { id: 5, marker: Marker.OPEN },
      { id: 6, marker: Marker.OPEN },
      { id: 7, marker: Marker.OPEN },
      { id: 8, marker: Marker.OPEN },
    ],
    onSquareClick: action('square clicked'),
  }
  return { state }
}

storiesOf('Board', module)
  .add('Initial board', () => {
    const { state } = setup();
    return <Board {...state} />
  })
  .add('With some markers', () => {
    const { state } = setup();
    state.squares[4].marker = Marker.X;
    state.squares[0].marker = Marker.O;
    state.squares[6].marker = Marker.X;
    state.squares[1].marker = Marker.O;
    return <Board {...state} />
  })
  .add('Player X Won', () => {
    const { state } = setup();
    state.squares[4].marker = Marker.X;
    state.squares[0].marker = Marker.O;
    state.squares[2].marker = Marker.X;
    state.squares[6].marker = Marker.X;
    state.squares[1].marker = Marker.O;
    state.playState = PlayState.WON;
    return <Board {...state} />
  })

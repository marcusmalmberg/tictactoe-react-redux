import React from 'react';
import { shallow } from 'enzyme';

import PlayState from '../constants/playState';
import Marker from '../constants/marker';
import Result from './Result';

describe('<Result />', () => {

  let props;

  beforeEach(() => {
    props = {
      player: Marker.X,
      playState: PlayState.RUNNING,
      restartGameClick: jest.fn(),
    }
  });

  describe('Current player', () => {
    it('is visible when game is RUNNING', () => {
      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('p.currentPlayer').length).toBe(1);
    });

    it('is not visible when game is WON', () => {
      props.playState = PlayState.WON;
      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('p.currentPlayer').length).toBe(0);
    });

    it('is not visible when game is TIED', () => {
      props.playState = PlayState.TIED;
      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('p.currentPlayer').length).toBe(0);
    });

    it('shows marker X when it is player X turn', () => {
      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('p.currentPlayer').text()).toEqual('Current player: X');
    });

    it('shows marker O when it is player O turn', () => {
      props.player = Marker.O;

      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('p.currentPlayer').text()).toEqual('Current player: O');
    });
  });

  describe('restart button', () => {
    it('is visible when game is WON', () => {
      props.playState = PlayState.WON;

      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('div.button').length).toBe(1);
    });

    it('is visible when game is TIED', () => {
      props.playState = PlayState.TIED;

      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('div.button').length).toBe(1);
    });

    it('is not visible when game is RUNNING', () => {
      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('div.button').length).toBe(0);
    });

    it('calls restartGame when restart button is clicked', () => {
      props.playState = PlayState.WON;

      const wrapper = shallow(<Result {...props}/>);

      wrapper.find('div.button').first().simulate('click');
      expect(props.restartGameClick.mock.calls.length).toBe(1);
    });

    it('have caption "Play again!"', () => {
      props.playState = PlayState.WON;

      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('div.button').text()).toEqual('Play again!');
    });
  });

  describe('game over', () => {
    it('displays Player X WON when player X won', () => {
      props.playState = PlayState.WON;

      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('p.game-over').text()).toEqual('Player X WON!');
    });

    it('displays Player O WON when player X won', () => {
      props.playState = PlayState.WON;
      props.player = Marker.O;

      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('p.game-over').text()).toEqual('Player O WON!');
    });

    it('displays The game was tied when game is TIED', () => {
      props.playState = PlayState.TIED;

      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('p.game-over').text()).toEqual('The game was TIED!');
    });

    it('is not visible when game is RUNNING', () => {
      const wrapper = shallow(<Result {...props}/>);

      expect(wrapper.find('p.game-over').length).toBe(0);
    });
  });
});

import React from 'react';
import { shallow } from 'enzyme';

import PlayState from '../constants/playState';
import Marker from '../constants/marker';
import Square from './Square';

describe('<Square />', () => {

  let props;

  beforeEach(() => {
    props = {
      square: { id: 0, marker: Marker.OPEN },
      onClick: jest.fn(),
      playState: PlayState.RUNNING,
    }
  });

  it('calls onClick when clicked', () => {

    const wrapper = shallow(<Square {...props}/>);

    wrapper.simulate('click');
    expect(props.onClick.mock.calls.length).toBe(1);
  });

  it('have content X when marker is X', () => {
    props.square.marker = Marker.X;

    const wrapper = shallow(<Square {...props}/>);

    expect(wrapper.find('div.Square').text()).toEqual('X');
  });

  it('have content O when marker is O', () => {
    props.square.marker = Marker.O;

    const wrapper = shallow(<Square {...props}/>);

    expect(wrapper.find('div.Square').text()).toEqual('O');
  });

  it('doesnt have any content when marker is OPEN', () => {
    const wrapper = shallow(<Square {...props}/>);

    expect(wrapper.find('div.Square').text()).toEqual('');
  });

  it('have class open when playstate is RUNNING and square is OPEN', () => {
    const wrapper = shallow(<Square {...props}/>);

    expect(wrapper.find('div.Square').hasClass('open')).toBe(true);
  });

  it('does not have class open when playstate is RUNNING and square is marked X', () => {
    props.square.marker = Marker.X;

    const wrapper = shallow(<Square {...props}/>);

    expect(wrapper.find('div.Square').hasClass('open')).toBe(false);
  });

});

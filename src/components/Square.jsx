import React, { PropTypes } from 'react';
import classNames from 'classnames';

import Marker, { markerToSymbol } from '../constants/marker';
import PlayState from '../constants/playState';
import './Square.css';

const Square = (props) => {
  const { square, playState } = props;
  const { onClick } = props;

  const classes = () => {
    return classNames(
      'Square',
      {
        'open': square.marker === Marker.OPEN && playState === PlayState.RUNNING,
        'marked-x': square.marker === Marker.X,
        'marked-o': square.marker === Marker.O
      }
    );
  }

  return (
    <div className={classes()} onClick={onClick} >
      {markerToSymbol(square.marker)}
    </div>
  );
}

Square.propTypes = {
  playState: PropTypes.string.isRequired,
  square: PropTypes.object.isRequired,
  onClick: PropTypes.func,
};

export default Square;

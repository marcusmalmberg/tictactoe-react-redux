import React from 'react';
import { shallow } from 'enzyme';

import PlayState from '../constants/playState';
import Marker from '../constants/marker';
import Board from './Board';
import Square from './Square';

describe('<Board />', () => {
  let props;

  beforeEach(() => {
    props = {
      playState: PlayState.RUNNING,
      onSquareClick: jest.fn(),
      squares: [
        { id: 0, marker: Marker.OPEN },
        { id: 1, marker: Marker.OPEN },
        { id: 2, marker: Marker.OPEN },
        { id: 3, marker: Marker.OPEN },
        { id: 4, marker: Marker.OPEN },
        { id: 5, marker: Marker.OPEN },
        { id: 6, marker: Marker.OPEN },
        { id: 7, marker: Marker.OPEN },
        { id: 8, marker: Marker.OPEN },
      ],
    }
  });

  it('renders nine <Square /> components', () => {
    const wrapper = shallow(<Board {...props} />);

    expect(wrapper.find(Square).length).toBe(9);
  });

  it('clicks open <Square />', () => {
    const wrapper = shallow(<Board {...props} />);

    wrapper.find(Square).first().simulate('click');
    expect(props.onSquareClick.mock.calls.length).toBe(1);
  });

  it('clicks Marked <Square />', () => {
    const wrapper = shallow(<Board {...props} />);
    props.squares[0].marker = Marker.X

    wrapper.find(Square).first().simulate('click');
    expect(props.onSquareClick.mock.calls.length).toBe(0);
  });
});

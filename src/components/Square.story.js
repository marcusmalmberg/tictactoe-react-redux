import React from 'react';
import { storiesOf, action, linkTo } from '@kadira/storybook';

import Square from './Square';
import Marker from '../constants/marker'
import PlayState from '../constants/playState'

const setup = () => {
  const state = {
    playState: PlayState.RUNNING,
    square: { id: 0, marker: Marker.OPEN },
    onClick: action('onClick'),
  }
  return { state }
}

storiesOf('Square', module)
  .add('Open (Game running)', () => {
    const { state } = setup();
    return <Square {...state} />
  })
  .add('Open (Game not running)', () => {
    const { state } = setup();
    state.playState = PlayState.WON
    return <Square {...state} />
  })
  .add('Marked X', () => {
    const { state } = setup();
    state.square.marker = Marker.X
    return <Square {...state} />
  })
  .add('Marked O', () => {
    const { state } = setup();
    state.square.marker = Marker.O
    return <Square {...state} />
  })
  .add('Without onClick', () => {
    const { state } = setup();
    state.onClick = null;
    return <Square {...state} />
  })

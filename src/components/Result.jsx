import React, { PropTypes } from 'react';

import { markerToSymbol } from '../constants/marker';
import PlayState from '../constants/playState';
import './Result.css';

const Result = (props) => {
  const { player, playState } = props;

  const resultMessage = () => {
    switch (playState) {
      case PlayState.WON:
        return (<p className="game-over">Player {markerToSymbol(player)} WON!</p>);

      case PlayState.TIED:
        return (<p className="game-over">The game was TIED!</p>);

      case PlayState.RUNNING:
      default:
        return (<p className="currentPlayer">Current player: {markerToSymbol(player)}</p>);
    }
  }

  return (
    <div className="Result">
      { resultMessage() }

      { playState !== PlayState.RUNNING &&
        <div className="button" onClick={props.restartGameClick}>Play again!</div>
      }
    </div>
  );
}

Result.propTypes = {
  player: PropTypes.string.isRequired,
  playState: PropTypes.string.isRequired,

  restartGameClick: PropTypes.func.isRequired,
};

export default Result;

import React from 'react';
import { storiesOf, action, linkTo } from '@kadira/storybook';

import Result from './Result';
import Marker from '../constants/marker'
import PlayState from '../constants/playState'

const setup = () => {
  const state = {
    playState: PlayState.RUNNING,
    player: Marker.X,
  }
  return { state }
}

storiesOf('Result', module)
  .add('Current player is X', () => {
    const { state } = setup();
    return <Result {...state} />
  })
  .add('Current player is O', () => {
    const { state } = setup();
    state.player = Marker.O;
    return <Result {...state} />
  })
  .add('Player X Won', () => {
    const { state } = setup();
    state.playState = PlayState.WON;
    return <Result {...state} />
  })
  .add('Game was tied', () => {
    const { state } = setup();
    state.playState = PlayState.TIED;
    return <Result {...state} />
  })

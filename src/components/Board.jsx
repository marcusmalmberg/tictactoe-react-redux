import React, { PropTypes } from 'react';

import Marker from '../constants/marker';
import PlayState from '../constants/playState';
import Square from './Square';
import './Board.css';

const Board = (props) => {
  const { squares, playState, onSquareClick } = props;

  return (
    <div className="Board">
      {squares.map((square, index) => {

        // Only pass onClick for open squares and while the game is running
        const onClick = () => {
          if (square.marker === Marker.OPEN && playState === PlayState.RUNNING)
            onSquareClick(square)
        }

        return <Square key={square.id} playState={playState} square={square} onClick={onClick} />
      })}
    </div>
  );
}

Board.propTypes = {
  playState: PropTypes.string.isRequired,
  squares: PropTypes.array.isRequired,
  onSquareClick: PropTypes.func.isRequired,
};

export default Board;

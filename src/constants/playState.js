var keyMirror = require('fbjs/lib/keyMirror');

const PlayState = keyMirror({
  RUNNING: null,
  WON: null,
  TIED: null,
});

export default PlayState;

var keyMirror = require('fbjs/lib/keyMirror');

const GameActions = keyMirror({
  MARK_SQUARE: null,
  RESTART_GAME: null,
});

export default GameActions;

var keyMirror = require('fbjs/lib/keyMirror');

const Marker = keyMirror({
  OPEN: null,
  X: null,
  O: null,
});

export const markerToSymbol = (marker) => {
  if (marker === Marker.X)
    return 'X';
  if (marker === Marker.O)
    return 'O';
  else
    return '';
}

export default Marker;

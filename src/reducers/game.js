import Marker from '../constants/marker';
import GameActions from '../constants/gameActions';
import PlayState from '../constants/playState';

const initialState = {
  playState: PlayState.RUNNING,
  player: Marker.X,

  // Keeping the squares in a sorted array with IDs that corresponds
  // to the index in the array enables us to simplify actions
  squares: [
    { id: 0, marker: Marker.OPEN },
    { id: 1, marker: Marker.OPEN },
    { id: 2, marker: Marker.OPEN },
    { id: 3, marker: Marker.OPEN },
    { id: 4, marker: Marker.OPEN },
    { id: 5, marker: Marker.OPEN },
    { id: 6, marker: Marker.OPEN },
    { id: 7, marker: Marker.OPEN },
    { id: 8, marker: Marker.OPEN },
  ],
}

const calculatePlayState = (squares, id, marker) => {

  const currentRow = Math.floor(id / 3);
  const currentColumn = id % 3;

  // Check horizontal
  const rowSquare1 = squares[(currentColumn + 1) % 3 + currentRow * 3];
  const rowSquare2 = squares[(currentColumn + 2) % 3 + currentRow * 3];

  if(rowSquare1.marker === marker && rowSquare2.marker === marker) {
    return PlayState.WON;
  }


  // Check vertical
  const colSquare1 = squares[currentColumn + (currentRow + 1) % 3 * 3];
  const colSquare2 = squares[currentColumn + (currentRow + 2) % 3 * 3];

  if(colSquare1.marker === marker && colSquare2.marker === marker) {
    return PlayState.WON;
  }


  // Check diagonal 1
  if(squares[0].marker === marker && squares[4].marker === marker && squares[8].marker === marker) {
    return PlayState.WON;
  }


  // Check diagonal 2
  if(squares[2].marker === marker && squares[4].marker === marker && squares[6].marker === marker) {
    return PlayState.WON;
  }

  // If at least one square is open we are still running
  for(let square of squares) {
    if (square.marker === Marker.OPEN) {
      return PlayState.RUNNING;
    }
  }

  // Otherwise it was a tie
  return PlayState.TIED;
}

const game = (state = initialState, action) => {
  switch (action.type) {

    case GameActions.RESTART_GAME:
      return initialState;

    case GameActions.MARK_SQUARE:
      const { square } = action

      // Mark Square
      const squares = [...state.squares]
      squares[square.id] = {
        id: square.id,
        marker: state.player,
      };

      // Find out if the game is over or if we are still running
      const playState = calculatePlayState(squares, square.id, state.player);

      // Toggle player if game is still running
      let player = state.player;
      if (playState === PlayState.RUNNING) {
        player = state.player ===  Marker.X ? Marker.O : Marker.X;
      }

      // Return new state
      return {
        ...state,
        playState,
        player,
        squares,
      };

    default:
      return state;
  }
}

export default game;

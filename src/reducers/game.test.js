import gameReducer from './game'
import GameActions from '../constants/gameActions'
import PlayState from '../constants/playState'
import Marker from '../constants/marker'

describe('GameReducer', () => {
  const setup = () => {
    const initialState = {
      playState: PlayState.RUNNING,
      player: Marker.X,
      squares: [
        { id: 0, marker: Marker.OPEN },
        { id: 1, marker: Marker.OPEN },
        { id: 2, marker: Marker.OPEN },
        { id: 3, marker: Marker.OPEN },
        { id: 4, marker: Marker.OPEN },
        { id: 5, marker: Marker.OPEN },
        { id: 6, marker: Marker.OPEN },
        { id: 7, marker: Marker.OPEN },
        { id: 8, marker: Marker.OPEN },
      ],
    }

    return { state: initialState }
  }

  it('should return the initial state', () => {
    const { state: initialState } = setup()
    expect(
      gameReducer(undefined, {})
    ).toEqual(
      initialState
    )
  })

  describe('RESTART_GAME', () => {
    it('resets state to initial state', () => {
      const { state: initialState } = setup();
      const { state } = setup();
      state.squares[5].marker = Marker.X;

      expect(
        gameReducer(state, {
          type: GameActions.RESTART_GAME,
        })
      ).toEqual(
        initialState
      )
    });
  });

  describe('MARK_SQUARE', () => {
    it('updates initialState', () => {
      const { state } = setup();
      state.squares[3].marker = Marker.X;
      state.player = Marker.O;

      expect(
        gameReducer(undefined, {
          type: GameActions.MARK_SQUARE,
          square: { id: 3, marker: Marker.OPEN }
        })
      ).toEqual(
        state
      )
    });

    describe('Win conditions', () => {
      describe('Player X Wins', () => {
        it('when placing third in top row', () => {
          const { state: preparedState } = setup();
          preparedState.squares[0].marker = Marker.X;
          preparedState.squares[1].marker = Marker.X;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 2, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)

          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.X)
        });

        it('when placing third in middle row', () => {
          const { state: preparedState } = setup();
          preparedState.squares[3].marker = Marker.X;
          preparedState.squares[4].marker = Marker.X;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 5, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)

          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.X)
        });

        it('when placing third in bottom row', () => {
          const { state: preparedState } = setup();
          preparedState.squares[6].marker = Marker.X;
          preparedState.squares[7].marker = Marker.X;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 8, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)

          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.X)
        });

        it('when placing third in left column', () => {
          const { state: preparedState } = setup();
          preparedState.squares[0].marker = Marker.X;
          preparedState.squares[3].marker = Marker.X;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 6, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)

          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.X)
        });

        it('when placing third in middle column', () => {
          const { state: preparedState } = setup();
          preparedState.squares[1].marker = Marker.X;
          preparedState.squares[4].marker = Marker.X;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 7, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)

          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.X)
        });

        it('when placing third in right column', () => {
          const { state: preparedState } = setup();
          preparedState.squares[2].marker = Marker.X;
          preparedState.squares[5].marker = Marker.X;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 8, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)

          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.X)
        });

        it('when placing third in diagonal 1', () => {
          const { state: preparedState } = setup();
          preparedState.squares[0].marker = Marker.X;
          preparedState.squares[4].marker = Marker.X;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 8, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)

          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.X)
        });

        it('when placing third in diagonal 2', () => {
          const { state: preparedState } = setup();
          preparedState.squares[2].marker = Marker.X;
          preparedState.squares[4].marker = Marker.X;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 6, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)

          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.X)
        });

      });

      describe('Player', () => {
        it('is toggled when game is Running', () => {
          const { state: preparedState } = setup();

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 2, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.RUNNING)
          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.O)
        });

        it('is not toggled when game is WON by X', () => {
          const { state: preparedState } = setup();
          preparedState.squares[0].marker = Marker.X;
          preparedState.squares[1].marker = Marker.X;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 2, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)
          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.X)
        });

        it('is not toggled when game is WON by O', () => {
          const { state: preparedState } = setup();
          preparedState.player = Marker.O;
          preparedState.squares[0].marker = Marker.O;
          preparedState.squares[1].marker = Marker.O;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 2, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)
          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.O)
        });
      });

      it('Game is Running when placing third in a mixed row', () => {
        const { state: preparedState } = setup();
        preparedState.squares[0].marker = Marker.O;
        preparedState.squares[1].marker = Marker.X;

        const action = {
          type: GameActions.MARK_SQUARE,
          square: { id: 2, marker: Marker.OPEN }
        }

        expect(gameReducer(preparedState, action))
          .toHaveProperty('playState', PlayState.RUNNING)
      });

      it('Game is Running when placing second in row', () => {
        const { state: preparedState } = setup();
        preparedState.squares[0].marker = Marker.X;

        const action = {
          type: GameActions.MARK_SQUARE,
          square: { id: 2, marker: Marker.OPEN }
        }

        expect(gameReducer(preparedState, action))
          .toHaveProperty('playState', PlayState.RUNNING)
      });

      describe('Player O Wins', () => {
        it('when placing third in top row', () => {
          const { state: preparedState } = setup();
          preparedState.player = Marker.O;
          preparedState.squares[0].marker = Marker.O;
          preparedState.squares[1].marker = Marker.O;

          const action = {
            type: GameActions.MARK_SQUARE,
            square: { id: 2, marker: Marker.OPEN }
          }

          expect(gameReducer(preparedState, action))
            .toHaveProperty('playState', PlayState.WON)

          expect(gameReducer(preparedState, action))
            .toHaveProperty('player', Marker.O)
        });
      });

      it('is TIED when all squares are filled', () => {
        const { state: preparedState } = setup();
        preparedState.squares[0].marker = Marker.O;
        preparedState.squares[1].marker = Marker.X;
        preparedState.squares[2].marker = Marker.O;
        preparedState.squares[3].marker = Marker.X;
        preparedState.squares[4].marker = Marker.O;
        preparedState.squares[5].marker = Marker.X;
        preparedState.squares[6].marker = Marker.X;
        preparedState.squares[7].marker = Marker.O;

        const action = {
          type: GameActions.MARK_SQUARE,
          square: { id: 8, marker: Marker.OPEN }
        }

        expect(gameReducer(preparedState, action))
          .toHaveProperty('playState', PlayState.TIED)
      });

      it('is WON when marking the last Square', () => {
        const { state: preparedState } = setup();
        preparedState.squares[0].marker = Marker.O;
        preparedState.squares[1].marker = Marker.X;
        preparedState.squares[2].marker = Marker.O;
        preparedState.squares[3].marker = Marker.X;
        preparedState.squares[4].marker = Marker.O;
        preparedState.squares[5].marker = Marker.X;
        preparedState.squares[6].marker = Marker.X;
        preparedState.squares[7].marker = Marker.O;
        preparedState.player = Marker.O;

        const action = {
          type: GameActions.MARK_SQUARE,
          square: { id: 8, marker: Marker.OPEN }
        }

        expect(gameReducer(preparedState, action))
          .toHaveProperty('playState', PlayState.WON)
      });

    });

  })
})

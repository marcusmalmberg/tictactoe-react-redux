import GameActions from '../constants/gameActions';

export const markSquare = (square) => (
  {
    type: GameActions.MARK_SQUARE,
    square,
  }
)

export const restartGame = (square) => (
  {
    type: GameActions.RESTART_GAME,
  }
)

import * as actions from '../actions';
import GameActions from '../constants/gameActions';
import Marker from '../constants/marker';

describe('actions', () => {
  it('should create an action to mark a square', () => {
    const square = { id: 1, marker: Marker.OPEN }
    const expectedAction = {
      type: GameActions.MARK_SQUARE,
      square
    }
    expect(actions.markSquare(square)).toEqual(expectedAction);
  });

  it('should create an action to restart game', () => {
    const expectedAction = {
      type: GameActions.RESTART_GAME,
    }
    expect(actions.restartGame()).toEqual(expectedAction);
  });
});

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';

import App, { App as PlainApp } from './App';
import Board from './components/Board';
import Result from './components/Result';
import configureStore from './store';
import PlayState from './constants/playState';
import Marker from './constants/marker';


describe('<App />', () => {
  it('renders without crashing', () => {
    const store = configureStore();
    const div = document.createElement('div');
    ReactDOM.render(<Provider store={store}><App /></Provider>, div);
  });

  it('contains <Board /> and <Result /> components', () => {
    const wrapper = shallow(
      <PlainApp
        squares={[]}
        playState={PlayState.RUNNING}
        player={Marker.X}
        markSquare={jest.fn()}
        restartGame={jest.fn()}
        />
    );

    expect(wrapper.find(Board).length).toBe(1);
    expect(wrapper.find(Result).length).toBe(1);
  });
});
